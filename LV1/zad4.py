'''
Zadatak 1.4.4 Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ song.txt.
Potrebno je napraviti rjecnik koji kao klju ˇ ceve koristi sve razli ˇ cite rije ˇ ci koje se pojavljuju u ˇ
datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (klju ˇ c) pojavljuje u datoteci. ˇ
Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih
'''

fhand=open('LV1/data/song.txt')

word_occurences={}
for line in fhand:
    line=line.rstrip()
    words=line.split()
    for word in words:
        word=word.rstrip(',')
        if word in word_occurences:
            word_occurences[word]=word_occurences[word]+1
        else:
             word_occurences[word]=1

oneOccurenceWords=0
print('Riječi koje se ponavljaju samo jednom su: ')
for word in word_occurences:
    if(word_occurences[word]==1):
        oneOccurenceWords=oneOccurenceWords+1
        print(word)

print('Ima ih: ',oneOccurenceWords)

fhand.close()