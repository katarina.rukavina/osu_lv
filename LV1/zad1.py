'''
Zadatak 1.4.1 Napišite program koji od korisnika zahtijeva unos radnih sati te koliko je placen ´
po radnom satu. Koristite ugradenu Python metodu ¯ input(). Nakon toga izracunajte koliko ˇ
je korisnik zaradio i ispišite na ekran. Na kraju prepravite rješenje na nacin da ukupni iznos ˇ
izracunavate u zasebnoj funkciji naziva ˇ total_euro.
'''


work_hours=int(input('Unesite broj radnih sati: '))
payPerHour=float(input('Unesite koliko ste plaćeni po satu: '))

#print('Korisnik je zaradio:',work_hours*payPerHour,'eura.')

def total_euro():
    return work_hours*payPerHour


print('Radni sati:', work_hours ,'h')
print('eura/h:',payPerHour)
print('Ukupno:', total_euro(),'eura')

