'''
Zadatak 1.4.2 Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
nekakvu ocjenu i nalazi se izmedu 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju ¯
sljedecih uvjeta: ´
>= 0.9 A
>= 0.8 B
>= 0.7 C
>= 0.6 D
< 0.6 F
Ako korisnik nije utipkao broj, ispišite na ekran poruku o grešci (koristite try i except naredbe).
Takoder, ako je broj izvan intervala [0.0 i 1.0] potrebno je ispisati odgovaraju ¯ cu poruku. 
'''
while True:
    try:
        percentage=float(input('Unesite broj između 0 i 1: '))
        if percentage<0 or percentage>1:
            print('Unjeli ste broj koji nije u intervalu.')
            continue
        elif percentage>=0.9:
            print('Ocjena je A.')
            break
        elif percentage>=0.8:
            print('Ocjena je B.')
            break
        elif percentage>=0.7:
            print('Ocjena je C.')
            break
        elif percentage>=0.6:
            print('Ocjena je D.')
            break
        elif percentage<0.6:
            print('Ocjena je F.')
            break

    except:
        print('Niste unjeli broj.')
