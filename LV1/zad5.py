'''
Zadatak 1.4.5 Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ SMSSpamCollection.txt
[1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke ozna ˇ cene kao ˇ spam, a neke kao ham.
Primjer dijela datoteke:
ham Yup next stop.
ham Ok lar... Joking wif u oni...
spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff
a) Izracunajte koliki je prosje ˇ can broj rije ˇ ci u SMS porukama koje su tipa ham, a koliko je ˇ
prosjecan broj rije ˇ ci u porukama koje su tipa spam. ˇ
b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?
'''
fhand=open('LV1/data/SMSSpamCollection.txt',encoding="utf8")

spamWords=0
spamNum=0
hamNum=0
hamWords=0
spamExclamation=0

for line in fhand:
    line=line.rstrip()
    words=line.split()
    if(words[0]=='spam'):
        spamNum=spamNum+1
        spamWords=spamWords+len(words)-1
        if line.endswith('!'):
            spamExclamation=spamExclamation+1
    elif(words[0]=='ham'):
        hamNum=hamNum+1
        hamWords=hamWords+len(words)-1

fhand.close()

print('Prosječna duljina spam poruke je: ',round(spamWords/spamNum))
print('Prosječna duljina ham poruke je: ',round(hamWords/hamNum))
print('Broj spam poruka koje završavaju s uskličnikom je',spamExclamation)



