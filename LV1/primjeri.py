# Primjer 1.1 Upotreba aritmetickog operatora
x = 23
print ( x )
x = x + 7
print ( x ) # komentar : ispis varijable na ekranu

# Primjer 1.2 Upotreba operatora usporedbe
x = 23
y = x > 10
print ( y )

# Primjer 1.3 Grananje if-else
x = 23
if x < 10:
    print (" x je manji od 10 ")
else :
    print (" x je veci ili jednak od 10 ")

# Primjer 1.4 Petlje while i for
i= 5
while i > 0:
    print ( i )
    i = i - 1
print (" Petlja gotova ")
for i in range (0 , 5 ):
    print ( i )

# Primjer 1.5 Liste
lstEmpty = [ ]
lstFriend = ['Marko','Luka', 'Pero']
lstFriend . append ('Ivan')
print ( lstFriend [0])
print ( lstFriend [0:1:2])
print ( lstFriend [ :2])
print ( lstFriend [1: ])
print ( lstFriend [1:3])

a = [1 , 2 , 3]
b = [4 , 5 , 6]
c = a + b
print ( c )
print ( max ( c) )
c[0] = 7
c . pop ()
for number in c:
    print ('List number ', number )
print ('Done !' )

# Primjer 1.6 Stringovi
fruit = 'banana' 
index = 0
count = 0
while index < len ( fruit ):
    letter = fruit [ index ]
    if letter == 'a' :
        count = count + 1
    print ( letter )
    index = index + 1
print ( count )
print ( fruit [0:3])
print ( fruit [0: ])
print ( fruit [2:6:1])
print ( fruit [0: -1])

line = 'Dobrodosli u nas grad'  
if( line . startswith ( 'Dobrodosli' ) ):
    print ('Prva rijec je Dobrodosli')
elif ( line . startswith ( 'dobrodosli' ) ):
    print ('Prva rijec je dobrodosli' )
line . lower ()
print ( line )
data = 'From : pero@yahoo . com'  
atpos = data . find ('@')
print ( atpos )

# Primjer 1.7 Tuple
letters = ('a' , 'b', 'c', 'd', 'e')
numbers = (1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 )
mixed = (1 , 'Hello' , 3.14 )
print ( letters [0])
print ( letters [1:4])
for letter in letters :
    print ( letter )

# Primjer 1.8 Rjecnik
hr_num = {'jedan' :1 , 'dva':2 , 'tri':3}
print ( hr_num )
print ( hr_num ['dva'])
hr_num ['cetiri'] = 4
print ( hr_num )

# Primjer 1.9 Korištenje modula
import random
import math
for i in range ( 10 ):
    x = random . random ()
    y = math . sin ( x )
    print ('Broj :' , x , 'Sin ( broj ) :', y )

# Primjer 1.10 Funkcije
def print_hello () :
    print (" Hello world ")

print_hello ()

# Primjer 1.11 Otvaranje tekstualne datoteke i citanje red po red
fhand = open ('LV1/data/example.txt')
for line in fhand :
    line = line . rstrip ()
    print ( line )
    words = line . split ()
fhand . close ()