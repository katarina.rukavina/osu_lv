'''
Zadatak 1.4.3 Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji ˇ
sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovaraju ˇ cu poruku.
'''

numbers=[]

while True:
    number=input('Unesite broj: ')
    if number.isnumeric():
        numbers.append(float(number))
    elif number=='Done': 
        break
    else:
        print('Niste unjeli broj.')

print('Korisnik je unjeo',len(numbers),'brojeva.')
print('Srednja vrijednost unesenih brojeva je',sum(numbers)/len(numbers))
print('Minimalna vrijednost unesenih brojeva je',min(numbers))
print('Maksimalna vrijednost unesenih brojeva je',max(numbers))
        
numbers.sort()
print('Sortirana lista: ')
print(numbers)
