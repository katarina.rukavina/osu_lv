'''
Primjer 8.1
from tensorflow import keras
from tensorflow . keras import layers
model = keras . Sequential ()
model . add ( layers . Input ( shape =(2 , ) ))
model . add ( layers . Dense (3 , activation =" relu ") )
model . add ( layers . Dense (1 , activation =" sigmoid ") )
model . summary ()
'''

'''
Primjer 8.3
model . compile ( loss =" categorical_crossentropy " ,
optimizer =" adam ",
metrics = [" accuracy " ,])
batch_size = 32
epochs = 20
history = model . fit ( X_train ,
y_train ,
batch_size = batch_size ,
epochs = epochs ,
validation_split = 0 . 1)
predictions = model . predict ( X_test )
score = model . evaluate ( X_test , y_test , verbose =0 )
'''

'''
Primjer 8.5
from keras . models import load_model
model . save (" FCN / ")
del model
model = load_model ( ’ FCN / ’)
model . summary ()
'''