'''
Zadatak 8.4.3 Napišite skriptu koja ce u ´ citati izgra ˇ denu mrežu iz zadatka 1. Nadalje, skripta ¯
treba ucitati sliku ˇ test.png sa diska. Dodajte u skriptu kod koji ce prilagoditi sliku za mrežu, ´
klasificirati sliku pomocu izgra ´ dene mreže te ispisati rezultat u terminal. Promijenite sliku ¯
pomocu nekog gra ´ fickog alata (npr. pomo ˇ cu Windows Paint-a nacrtajte broj 2) i ponovo pokrenite ´
skriptu. Komentirajte dobivene rezultate za razlicite napisane znamenke.
'''

import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix
from keras import models
from PIL import Image



model = models.load_model ( 'model.h5')

image = Image.open("LV8/test.png")
image = image.convert("L")
image = image.resize((28, 28))

image_array = np.array(image)
image_array = image_array.astype("float32") / 255
image_array = np.expand_dims(image_array, axis=0)



predictions = model.predict(image_array)
print(predictions)
predicted_class = np.argmax(predictions, axis=1)


plt.imshow(image)
plt.title(f'Stvarni broj: 9, broj predviđen modelom: {predicted_class[0]}')
plt.show()

