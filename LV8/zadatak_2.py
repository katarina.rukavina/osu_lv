
'''
Zadatak 8.4.2 Napišite skriptu koja ce u ´ citati izgra ˇ denu mrežu iz zadatka 1 i MNIST skup ¯
podataka. Pomocu matplotlib biblioteke potrebno je prikazati nekoliko loše klasi ´ ficiranih slika iz
skupa podataka za testiranje. Pri tome u naslov slike napišite stvarnu oznaku i oznaku predvidenu ¯
mrežom.
'''
import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix
from keras import models


model = models.load_model ( 'model.h5')

num_classes = 10

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()


# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


predictions = model.predict(x_test_s)

y_pred = np.argmax(predictions, axis=1)
y_true = np.argmax(y_test_s, axis=1)

missclassified = np.where(y_pred != y_true)[0]
plt.subplots(3, 3, figsize=(10, 10))
for i in range(9):
    plt.subplot(3, 3, i + 1)
    plt.imshow(x_test[missclassified[i]], cmap='gray')
    plt.title(f"True: {y_true[missclassified[i]]}, Pred: {y_pred[missclassified[i]]}")
    plt.axis('off')
plt.show()