'''
Zadatak 5.5.1 Skripta zadatak_1.py generira umjetni binarni klasifikacijski problem s dvije
ulazne velicine. Podaci su podijeljeni na skup za u ˇ cenje i skup za testiranje modela. ˇ
a) Prikažite podatke za ucenje u ˇ x1 −x2 ravnini matplotlib biblioteke pri cemu podatke obojite ˇ
s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
cmap kojima je moguce de ´ finirati boju svake klase.
b) Izgradite model logisticke regresije pomo ˇ cu scikit-learn biblioteke na temelju skupa poda- ´
taka za ucenje. ˇ
c) Pronadite u atributima izgra ¯ denog modela parametre modela. Prikažite granicu odluke ¯
naucenog modela u ravnini ˇ x1 − x2 zajedno s podacima za ucenje. Napomena: granica ˇ
odluke u ravnini x1 −x2 definirana je kao krivulja: θ0 +θ1x1 +θ2x2 = 0.
d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgra ´ denog modela logisti ¯ cke ˇ
regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izra ˇ cunate to ˇ cnost, ˇ
preciznost i odziv na skupu podataka za testiranje.
e) Prikažite skup za testiranje u ravnini x1 −x2. Zelenom bojom oznacite dobro klasi ˇ ficirane
primjere dok pogrešno klasificirane primjere oznacite crnom bojom.
'''




import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn . linear_model import LogisticRegression

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay,accuracy_score,precision_score,recall_score


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a)
plt.scatter(x=X_train[:,0],y=X_train[:,1],c=y_train,cmap='viridis',label='Training')
plt.scatter(x=X_test[:,0],y=X_test[:,1],c=y_test,cmap='viridis',marker='x',label='Test')
plt.title('x1-x2 ravnina')
plt.legend()
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

#b)
LogRegression_model = LogisticRegression ()
LogRegression_model . fit ( X_train , y_train )

#c)
t0=LogRegression_model.intercept_[0]
t1 = LogRegression_model.coef_[0,0]
t2 = LogRegression_model.coef_[0,1]
decision=(-t1*X_train[:,0]-t0)/t2
plt.scatter(x=X_train[:,0],y=X_train[:,1],c=y_train,cmap='viridis',label='Training')
plt.plot(X_train[:,0],decision)
plt.title('x1-x2 ravnina')
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

#d)
y_test_p=LogRegression_model.predict(X_test)
disp = ConfusionMatrixDisplay ( confusion_matrix ( y_test, y_test_p) )
disp . plot ()
plt . show ()
print ("Tocnost : " , accuracy_score ( y_test , y_test_p ) )
print("Preciznost : ", precision_score(y_test, y_test_p))
print("Odziv : ", recall_score(y_test, y_test_p))

#e)
plt.figure()
plt.scatter(X_test[y_test == y_test_p][:, 0], X_test[y_test == y_test_p][:, 1], marker='o', c='g',label='Točno klasificirano')
plt.scatter(X_test[y_test != y_test_p][:, 0], X_test[y_test != y_test_p][:, 1], marker='o', c='black',label='Netočno klasificirano')
plt.title('Test podaci x1-x2 ravnina')
plt.xlabel('x1')
plt.ylabel('x2')
plt.legend()
plt.show()
