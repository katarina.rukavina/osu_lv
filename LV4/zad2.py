'''
Zadatak 4.5.2 Na temelju rješenja prethodnog zadatka izradite model koji koristi i kategoricku ˇ
varijable „Fuel Type“ kao ulaznu velicinu. Pri tome koristite 1-od-K kodiranje kategori ˇ ckih ˇ
velicina. Radi jednostavnosti nemojte skalirati ulazne veli ˇ cine. Komentirajte dobivene rezultate. ˇ
Kolika je maksimalna pogreška u procjeni emisije C02 plinova u g/km? O kojem se modelu
vozila radi?
'''

from sklearn . model_selection import train_test_split
import pandas as pd 
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error,mean_squared_error,root_mean_squared_error,mean_absolute_percentage_error,r2_score
import matplotlib.pyplot as plt
from sklearn . preprocessing import OneHotEncoder
import numpy as np

data=pd.read_csv('LV4/data/data_C02_emission.csv')

x=data[['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]
y=data[['CO2 Emissions (g/km)']]

ohe = OneHotEncoder()
X_encoded = ohe . fit_transform ( data [ ['Fuel Type'] ]) . toarray ()

x = pd.concat([pd.DataFrame(X_encoded, columns=ohe.get_feature_names_out(['Fuel Type'])), x], axis=1)

x_train , x_test , y_train , y_test = train_test_split (x , y , test_size = 0.2 , random_state =1 )

linearModel = lm . LinearRegression ()
linearModel . fit ( x_train, y_train )

print('Koeficijenti modela: ')
print(linearModel.coef_)
print(linearModel.intercept_)
print('Jednadžba modela je: ')
feature_cols=x.columns

for f in range(len(feature_cols)):
    print("{0}* {1} + ".format(linearModel.coef_[0][f],feature_cols[f]))
print(linearModel.intercept_[0])


y_test_predicted=linearModel.predict(x_test)
print('Srednja kvadratna pogreška:',mean_squared_error(y_test, y_test_predicted))
print('Korijen od srednje kvadratne pogreške:',root_mean_squared_error(y_test, y_test_predicted))
print('Srednja apsolutna pogreška:',mean_absolute_error(y_test, y_test_predicted))
print('Srednja apsolutna postotna pogreška:',mean_absolute_percentage_error(y_test, y_test_predicted))
print('Koeficijent determinacije:',r2_score(y_test, y_test_predicted))

y_predicted=linearModel.predict(x)
print('Maksimalna pogreška u procjeni emisije CO2 plinova je:',np.max(np.abs(y_predicted-y)))
print('Model vozila je ',data.iloc[np.argmax(np.abs(y_predicted-y))]['Model'])



