'''
Zadatak 4.5.1 Skripta zadatak_1.py ucitava podatkovni skup iz ˇ data_C02_emission.csv.
Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju ostalih numerickih ulaznih veli ˇ cina. Detalje oko ovog podatkovnog skupa mogu se prona ˇ ci u 3. ´
laboratorijskoj vježbi.
a) Odaberite željene numericke veli ˇ cine speci ˇ ficiranjem liste s nazivima stupaca. Podijelite
podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%. ˇ
b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova ´
o jednoj numerickoj veli ˇ cini. Pri tome podatke koji pripadaju skupu za u ˇ cenje ozna ˇ cite ˇ
plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom. ˇ
c) Izvršite standardizaciju ulaznih velicina skupa za u ˇ cenje. Prikažite histogram vrijednosti ˇ
jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja ˇ
transformirajte ulazne velicine skupa podataka za testiranje. ˇ
d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
povežite ih s izrazom 4.6.
e) Izvršite procjenu izlazne velicine na temelju ulaznih veli ˇ cina skupa za testiranje. Prikažite ˇ
pomocu dijagrama raspršenja odnos izme ´ du stvarnih vrijednosti izlazne veli ¯ cine i procjene ˇ
dobivene modelom.
f) Izvršite vrednovanje modela na nacin da izra ˇ cunate vrijednosti regresijskih metrika na ˇ
skupu podataka za testiranje.
g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj ¯
ulaznih velicina?
'''


from sklearn . model_selection import train_test_split
from sklearn . preprocessing import StandardScaler
import pandas as pd 
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error,mean_squared_error,root_mean_squared_error,mean_absolute_percentage_error,r2_score
import matplotlib.pyplot as plt



data=pd.read_csv('LV4/data/data_C02_emission.csv')

#a)
x=data[['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]
y=data[['CO2 Emissions (g/km)']]
x_train , x_test , y_train , y_test = train_test_split (x , y , test_size = 0.2 , random_state =1 )

#b)
plt.scatter(x_train['Engine Size (L)'],y_train,color='blue')
plt.scatter(x_test['Engine Size (L)'],y_test,color='red')
plt.title('Ovisnost emisije CO2 o veličini motora')
plt.legend(['train', 'test'])
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

#c)
sc= StandardScaler()
x_train_n = sc . fit_transform ( x_train )
plt.hist(x_train['Cylinders'])
plt.title('Histogram cilindara prije skaliranja')
plt.show()
plt.hist(x_train_n[1])
plt.title('Histogram clindara poslije skaliranja')
plt.show()
x_test_n = sc . transform ( x_test )

#d)
linearModel = lm . LinearRegression ()
linearModel . fit ( x_train_n , y_train )
print('Koeficijenti modela: ')
print(linearModel.coef_)
print(linearModel.intercept_)
print('Jednadžba modela je: ')
feature_cols=x.columns

for f in range(len(feature_cols)):
    print("{0}* {1} + ".format(linearModel.coef_[0][f],feature_cols[f]))
print(linearModel.intercept_[0])


#e)
y_test_predicted=linearModel.predict(x_test_n)
plt.scatter(y_test,y_test_predicted)
plt.title('Odnos između stvarne izlazne veličine i izlazne veličine dobivene modelom')
plt.xlabel('Stvarna vrijednost')
plt.ylabel('Procjenjena vrijednost')
plt.show()

#f)
print('Srednja kvadratna pogreška:',mean_squared_error(y_test, y_test_predicted))
print('Korijen od srednje kvadratne pogreške:',root_mean_squared_error(y_test, y_test_predicted))
print('Srednja apsolutna pogreška:',mean_absolute_error(y_test, y_test_predicted))
print('Srednja apsolutna postotna pogreška:',mean_absolute_percentage_error(y_test, y_test_predicted))
print('Koeficijent determinacije:',r2_score(y_test, y_test_predicted))

#g)
#??????