'''
Zadatak 2.4.1 Pomocu funkcija ´ numpy.array i matplotlib.pyplot pokušajte nacrtati sliku
2.3 u okviru skripte zadatak_1.py. Igrajte se sa slikom, promijenite boju linija, debljinu linije i
sl.
'''

import numpy as np
import matplotlib.pyplot as plt 

x=np.array([1,3,3,2,1])
y=np.array([1,1,2,2,1])
plt.plot(x,y,'b',linewidth=1,marker='.',markersize=5)
plt.axis([0.0,4.0,0.0,4.0])
plt.xlabel('x os')
plt.ylabel('y os')
plt.title('Primjer')
plt.show()


plt.plot(x,y,'g',linewidth=3,marker='.',markersize=5)
plt.axis([0.0,4.0,0.0,4.0])
plt.xlabel('x os')
plt.ylabel('y os')
plt.title('Primjer')
plt.show()


x=np.array([1,3,3,2,1])
y=np.array([1,1,2,2,1])
plt.plot(x,y,'r',linewidth=5,marker='.',markersize=5)
plt.axis([0.0,4.0,0.0,4.0])
plt.xlabel('x os')
plt.ylabel('y os')
plt.title('Primjer')
plt.show()