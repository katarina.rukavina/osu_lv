'''
Zadatak 2.4.2 Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja ˇ data pri cemu je u ˇ
prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci´
stupac polja je masa u kg
a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja? ˇ
b) Prikažite odnos visine i mase osobe pomocu naredbe ´ matplotlib.pyplot.scatter.
c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom ˇ
podatkovnom skupu.
e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
ind = (data[:,0] == 1)
'''
import numpy as np
import matplotlib . pyplot as plt

data=np.genfromtxt('LV2/data/data.csv',delimiter=',')

data=data[1:]
person_number=np.shape(data)[0]
print('Istraživanje je izvršeno na',person_number,'osoba.')

height=data[:,1]
weight=data[:,2]
plt.scatter(weight,height)
plt.xlabel('Visina')
plt.ylabel('Masa')
plt.title('Odnos visine i mase')
plt.show()

height=data[::50,1]
weight=data[::50,2]
plt.scatter(weight,height)
plt.xlabel('Visina')
plt.ylabel('Masa')
plt.title('Odnos visine i mase')
plt.show()

print('Minimalna vrijednost visine je',np.min(data[:,1]),'cm.')
print('Maksimalna vrijednost visine je',np.max(data[:,1]),'cm.')
print('Srednja vrijednost visine je',np.mean(data[:,1]),'cm.')


ind = (data[:,0] == 1)
print('Minimalna vrijednost visine za muškarce je',np.min(data[ind,1]),'cm.')
print('Maksimalna vrijednost visine za muškarce je',np.max(data[ind,1]),'cm.')
print('Srednja vrijednost visine za muškarce je',np.mean(data[ind,1]),'cm.')

ind = (data[:,0] == 0)
print('Minimalna vrijednost visine za žene je',np.min(data[ind,1]),'cm.')
print('Maksimalna vrijednost visine za žene je',np.max(data[ind,1]),'cm.')
print('Srednja vrijednost visine za žene je',np.mean(data[ind,1]),'cm.')
