'''
Zadatak 2.4.3 Skripta zadatak_3.py ucitava sliku ’ ˇ road.jpg’. Manipulacijom odgovarajuce´
numpy matrice pokušajte:
a) posvijetliti sliku,
b) prikazati samo drugu cetvrtinu slike po širini, ˇ
c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu,
d) zrcaliti sliku.
'''



import numpy as np
import matplotlib . pyplot as plt


img = plt . imread ("LV2/data/road.jpg")
img = img [ :,:,0]. copy ()
plt.figure()
plt . imshow ( img , cmap ="gray")
plt . show ()


plt.figure()
plt . imshow ( img , cmap ="gray",alpha=0.25)
plt . show ()

img_split=np.hsplit(img,4)
plt.figure()
plt . imshow ( img_split[1] , cmap ="gray",alpha=0.25)
plt . show ()

img_rotate=np.rot90(img)
plt.figure()
plt . imshow ( img_rotate , cmap ="gray",alpha=0.25)
plt . show ()

img_mirror=np.fliplr(img)
plt.figure()
plt . imshow ( img_mirror , cmap ="gray",alpha=0.25)
plt . show ()
