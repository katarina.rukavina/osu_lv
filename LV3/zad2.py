'''
Zadatak 3.4.2 Napišite programski kod koji ce prikazati sljede ´ ce vizualizacije: ´
a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz. ´
b) Pomocu dijagrama raspršenja prikažite odnos izme ´ du gradske potrošnje goriva i emisije ¯
C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu¯
velicina, obojite to ˇ ckice na dijagramu raspršenja s obzirom na tip goriva. ˇ
c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip ´
goriva. Primjecujete li grubu mjernu pogrešku u podacima? ´
d) Pomocu stup ´ castog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu ˇ
groupby.
e) Pomocu stup ´ castog grafa prikažite na istoj slici prosje ˇ cnu C02 emisiju vozila s obzirom na ˇ
broj cilindara.
'''


import pandas as pd
import matplotlib . pyplot as plt


data = pd . read_csv ( 'LV3/data/data_C02_emission.csv')
cols=data.select_dtypes(include='object').columns.to_list()
data[cols]=data[cols].astype('category')

#a) 
plt.hist(data['CO2 Emissions (g/km)'],bins=20)
plt.title('CO2 Emissions (g/km) histogram')
plt.xlabel('CO2 Emissions (g/km)')
plt.ylabel('Cars')
plt.show()

#b)

data . plot . scatter ( 
                    x='Fuel Consumption City (L/100km)' ,
                    y='CO2 Emissions (g/km)',
                    c='Fuel Type',cmap='viridis')
plt.title('Odnos između gradske potrošnje vozila i emisije C02 plinova')
plt.show()

#c)
data . boxplot ( column =['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')
plt . show ()

#d)
carsByFuelType = data.groupby('Fuel Type').size()
carsByFuelType.plot(kind='bar')
plt.title('Number of cars by fuel type')
plt.ylabel('Number of cars')
plt.show()

#e)
c02ByCylinders=data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
c02ByCylinders.plot(kind='bar')
plt.ylabel('Average CO2 emission(g/km)')
plt.title('Average CO2 emission(g/km) by number of cylinders')
plt.show()

