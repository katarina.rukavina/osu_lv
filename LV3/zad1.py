'''
Zadatak 3.4.1 Skripta zadatak_1.py ucitava podatkovni skup iz ˇ data_C02_emission.csv.
Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljede ´ ca pitanja: ´
a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili ˇ
duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke veli ˇ cine konvertirajte u tip ˇ
category.
b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal: ´
ime proizvoda¯ ca, model vozila i kolika je gradska potrošnja. ˇ
c) Koliko vozila ima velicinu motora izme ˇ du 2.5 i 3.5 L? Kolika je prosje ¯ cna C02 emisija ˇ
plinova za ova vozila?
d) Koliko mjerenja se odnosi na vozila proizvoda¯ ca Audi? Kolika je prosje ˇ cna emisija C02 ˇ
plinova automobila proizvoda¯ ca Audi koji imaju 4 cilindara? ˇ
e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na ˇ
broj cilindara?
f) Kolika je prosjecna gradska potrošnja u slu ˇ caju vozila koja koriste dizel, a kolika za vozila ˇ
koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva? ´
h) Koliko ima vozila ima rucni tip mjenja ˇ ca (bez obzira na broj brzina)? ˇ
i) Izracunajte korelaciju izme ˇ du numeri ¯ ckih veli ˇ cina. Komentirajte dobiveni rezultat.
'''

import pandas as pd


data = pd . read_csv ( 'LV3/data/data_C02_emission.csv')

#a)
print('Ovaj dataframe ima',len(data),'mjerenja.')
print('Tipovi podataka svake veličine : ')
print(data.dtypes)
if data.isnull().sum().sum()>0:
    data=data.dropna(axis=0)
    data=data.reset_index(drop=True)
    print('Postoje izostale vrijednosti.')
else:
    print('Ne postoje izostale vrijednosti.')

if data.duplicated().sum().sum()>0:
    data=data.drop_duplicates()
    data=data.reset_index(drop=True)
    print('Postoje duplicirane vrijednosti.')
else:
    print('Ne postoje duplicirane vrijednosti.')

cols=data.select_dtypes(include='object').columns.to_list()
data[cols]=data[cols].astype('category')

#b)
sortedByCityConsumption=data.sort_values(by=['Fuel Consumption City (L/100km)'],ascending=False)
print('Tri autombila s najvećom gradskom potrošnjom su: ')
print(sortedByCityConsumption.head(3)[['Make','Model','Fuel Consumption City (L/100km)']])
print('Tri autombila s najmanjom gradskom potrošnjom su: ')
print(sortedByCityConsumption.tail(3)[['Make','Model','Fuel Consumption City (L/100km)']])

#c)
specificEngineSize=data[data['Engine Size (L)'].between(2.5,3.5,inclusive='neither')]
print(len(specificEngineSize),'vozila ima veličinu motora između 2.5 i 3.5.')
print('Prosječna emisija C02 plinova za ova vozila je',specificEngineSize['CO2 Emissions (g/km)'].mean(),'g/km.')

#d)
print(len(data[data['Make']=='Audi']),'mjerenja se odnosi na vozila proizvođača Audi.')
audi4cylinders=data[(data['Make']=='Audi') & (data['Cylinders']==4)]
print('Prosječna emisija C02 plinova za vozila marke Audi koja imaju 4 cilindra je',audi4cylinders['CO2 Emissions (g/km)'].mean(),'g/km.')

#e)
print('Broj vozila s obzirom na broj cilindara: ',data['Cylinders'].value_counts())
averageByCylinders=data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
print('Prosječna emisija C02 plinova s obzirom na broj cilindara: ',averageByCylinders)

#f)
print('Prosječna gradska potrošnja za vozila koja koriste dizel je: ',data[data['Fuel Type']=='D']['Fuel Consumption City (L/100km)'].mean(),'L/100km.')
print('Prosječna gradska potrošnja za vozila koja koriste regularni benzin je: ',data[data['Fuel Type']=='X']['Fuel Consumption City (L/100km)'].mean(),'L/100km.')
print('Medijalna vrijednost gradske potrošnje za vozila koja koriste dizel je: ',data[data['Fuel Type']=='D']['Fuel Consumption City (L/100km)'].median(),'L/100km.')
print('Medijalna vrijednost gradske potrošnje za vozila koja koriste regularni benzin je: ',data[data['Fuel Type']=='X']['Fuel Consumption City (L/100km)'].median(),'L/100km.')

#g)
row=data[(data['Cylinders']==4) & (data['Fuel Type']=='D')]['Fuel Consumption City (L/100km)'].idxmax()
print('Vozilo sa 4 cilindra i dizelskim motorom koje ima najveću gradsku potrošnju je: ',data.iloc[row])

#h)
manualCars=data[data['Transmission'].str.startswith('M')]
print('Vozila sa ručnim tipom mjenjača ima',len(manualCars),'.')

#i)
print('Korelacija numeričkih veličina:',data.corr(numeric_only=True))

