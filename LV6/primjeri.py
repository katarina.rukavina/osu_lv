'''
Primjer 6.1
from sklearn . neighbors import KNeighborsClassifier
from sklearn import svm
# inicijalizacija i ucenje KNN modela
KNN_model = KNeighborsClassifier ( n_neighbors = 5 )
KNN_model . fit ( X_train_n , y_train )
# inicijalizacija i ucenje SVM modela
SVM_model = svm . SVC ( kernel =’ rbf ’, gamma = 1 , C=0 . 1 )
SVM_model . fit ( X_train_n , y_train )
# predikcija na skupu podataka za testiranje
y_test_p_KNN = KNN_model . predict ( X_test )
y_test_p_SVM = SVM_model . predict ( X_test )
'''

'''
Primjer 6.2
from sklearn . model_selection import cross_val_score
model = svm . SVC ( kernel =’ linear ’, C=1 , random_state =42 )
scores = cross_val_score ( clf , X_train , y_train , cv =5 )
print ( scores )
'''

'''
Primjer 6.3
from sklearn . svm import SVC
from sklearn . preprocessing import StandardScaler
from sklearn . pipeline import Pipeline
from sklearn . pipeline import make_pipeline
from sklearn . model_selection import GridSearchCV
param_grid = {’ model__C ’: [10 , 100 , 100 ],
’ model__gamma ’: [10 , 1 , 0.1 , 0 . 01 ]}
svm_gscv = GridSearchCV ( pipe , param_grid , cv =5 , scoring =’ accuracy ’,
n_jobs = -1 )
svm_gscv . fit ( X_train , y_train )
print ( svm_gscv . best_params_ )
print ( svm_gscv . best_score_ )
print ( svm_gscv . cv_results_ )
'''